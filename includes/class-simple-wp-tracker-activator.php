<?php

/**
 * Fired during plugin activation
 *
 * @link       http://presstigers.com/
 * @since      1.0.0
 *
 * @package    Simple_Wp_Tracker
 * @subpackage Simple_Wp_Tracker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Simple_Wp_Tracker
 * @subpackage Simple_Wp_Tracker/includes
 * @author     PressTigers <support@presstigers.com>
 */
class Simple_Wp_Tracker_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
